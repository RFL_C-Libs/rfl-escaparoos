#ifndef INCLUDE_ESCAPAROOS_H
#define INCLUDE_ESCAPAROOS_H

/**
 * @file color.h
 * @author R Landau (rflandau@pm.me)
 * @brief Implements macros of the SGR parameters as defined here: https://en.wikipedia.org/wiki/ANSI_escape_code
 * @version 1.1
 * @date 2024-01-14
 * 
 * @copyright Nah, man
 * 
 */

// Common operators as macros ##################################################

// SGR params ------------------------------------------------------------------

#ifdef COLORIZE
#define BOLD(t) "\e[1m" t "\e[0m"
#define DIM(t) "\e[2m" t "\e[0m"
#define FAINT(t) "\e[2m" t "\e[0m"
#define ITALIC(t) "\e[3m" t "\e[0m"
#define UNDERLINE(t) "\e4m" t "\e[0m"
#define SLOW_BLINK(t) "\e5m" t /**< needs to be followed by a RESET*/
#define RAPID_BLINK(t) "\e6m" t /** needs to be followed by a RESET*/
#define INVERT(t) "\e7m" t /**< needs to be followed by a RESET*/
#define CONCEAL(t) "\e8m" t /**< needs to be followed by a RESET*/
#define STRIKE(t) "\e9m" t "\e[0m"
#define FRAKTUR(t) "\e20m" t "\e[0m"
#define DOUBLE_UNDERLINE(t) "\e21m" t "\e[0m"
#define SUPERSCRIPT(t) "\e73m" t "\e[0m"
#define SUBSCRIPT(t) "\e74m" t "\e[0m"

#define RESET(t) "\e0m"

// SGR 3-bit colors ------------------------------------------------------------
#define BLACK(t) "\e[30m" t "\e[0m"
#define RED(t) "\e[31m" t "\e[0m"
#define GREEN(t) "\e[32m" t "\e[0m"
#define YELLOW(t) "\e[33m" t "\e[0m"
#define BLUE(t) "\e[34m" t "\e[0m"
#define MAGENTA(t) "\e[35m" t "\e[0m"
#define CYAN(t) "\e[36m" t "\e[0m"
#define WHITE(t) "\e[37m" t "\e[0m"

// SGR RGB ---------------------------------------------------------------------
#define FG_RGB(t, r, g, b) "\e[38;2;" r ";" g ";" b "m" t "\e[0m"
#define BG_RGB(t, r, g, b) "\e[48;2;" r ";" g ";" b "m" t "\e[0m"
#else
#define BOLD(t) t
#define DIM(t) t
#define FAINT(t) t
#define ITALIC(t) t
#define UNDERLINE(t) t
#define SLOW_BLINK(t) t 
#define RAPID_BLINK(t) t 
#define INVERT(t) t 
#define CONCEAL(t) t 
#define STRIKE(t) t
#define FRAKTUR(t) t
#define DOUBLE_UNDERLINE(t) t
#define SUPERSCRIPT(t) t
#define SUBSCRIPT(t) t

#define RESET(t) ""

// SGR 3-bit colors ------------------------------------------------------------
#define BLACK(t)t
#define RED(t) t
#define GREEN(t) t
#define YELLOW(t) t
#define BLUE(t) t
#define MAGENTA(t) t
#define CYAN(t) t
#define WHITE(t) t

// SGR RGB ---------------------------------------------------------------------
#define FG_RGB(t, r, g, b) t
#define BG_RGB(t, r, g, b) t
#endif

#endif