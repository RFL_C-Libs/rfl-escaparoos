# RFL Escaparoos

A library for applying ANSI SGR effects (https://en.wikipedia.org/wiki/ANSI_escape_code). Pure macros, just add the .h file to your project!

# Features

- [x] colorizing
- [x] bold, italics, strike-through, underscore, and fraktur
- [x] just macros
- [x] togglable

# Install

To start using this library, drop the escaparoos.h file into your project, `#include` it, and compile with `-DCOLORIZE`.

# Usage

```c
#define COLORIZE // or compile with -DCOLORIZE

#include "escaparoos.h"
...

int main(argc, char* argv[]){
    printf(RED("I will be red!"));
    printf("Just the end will be %s", BOLD(BLUE("bold blue")));
    char* you_can_use_rgb_too = FG_RGB("rgb!", 50, 100, 200);
}
```